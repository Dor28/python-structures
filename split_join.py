
def main():
    s = str(input())
    print(" ".join(s.split(" ")[::-1])[::-1])


if __name__ == "__main__":
    main()
