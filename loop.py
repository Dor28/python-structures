"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""
import math


def main():
    """Factorial calculation."""
    n = int(input())
    print(math.factorial(n))


if __name__ == "__main__":
    main()
